'use strict';
const repository = require('../repository/users.repository');
const passwordEncrypt = require("./encrypts-password.service");

const getDataOfBody = (req) => {
    return {
        id: undefined,
        name: req.body.name,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }
};


const responseDto = (response) => {
    return {
        name: response.name,
        username: response.username,
        email: response.email
    }
};


exports.create = async (req, res, next) => {
    try {
        const passwordEncrypted = await passwordEncrypt.encryptPassword(req.body.password);
        const user = getDataOfBody(req);
        user.password = passwordEncrypted;
        const response = await repository.create(user);
        res.status(201).send(responseDto(response));
    } catch (e) {
        errorInternalServer(res);
    }
};
exports.update = async (req, res, next) => {
    const {id} = req.userContext;
    const user = getDataOfBody(req);
    try {
        let response = await repository.findUserById(id);
        if (response) {
            user.id = id;
            user.password = await passwordEncrypt.encryptPassword(user.password);
            const response = await repository.update(user);
            res.status(200).send(...response.filter(value => value).map(responseDto));
        } else {
            res.status(404).send({
                message: "Usuários não encontrado!"
            });
        }
    } catch (e) {
        errorInternalServer(res);
    }
};

exports.findAllUsers = async (req, res, next) => {
    try {
        const response = await repository.findAllUsers();
        res.status(200).send(response.map(responseDto));
    } catch (e) {
        errorInternalServer(res);
    }
};

exports.findUserById = async (req, res, next) => {
    const {id} = req.userContext;
    try {
        const response = await repository.findUserById(id);
        if (response) {
            res.status(200).send(responseDto(response));
        } else {
            responseUserNotFound(res, id);
        }
    } catch (e) {
        errorInternalServer(res);
    }
};

exports.deleteById = async (req, res, next) => {
    const {id} = req.userContext;
    try {
        let user = await repository.findUserById(id);
        if (user) {
            await repository.deleteById(id);
            res.status(200).send();
        } else {
            responseUserNotFound(res, id);
        }
    } catch (e) {
        errorInternalServer(res);
    }
};

const errorInternalServer = (res) => {
    res.status(500).send({
        message: "Erro interno do servidor"
    });
};

const responseUserNotFound = (res, id) => {
    res.status(404).send({
        message: `Não foi possivel encontrar usuário com id = ${id}`
    });
};