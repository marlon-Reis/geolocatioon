'use strict';


const repository = require('../repository/users.repository');
const passwordEncrypt = require("./encrypts-password.service");
const security = require("../security/authorize.security");

exports.authentication = async (req, res, next) => {
    try {
        const {email, password} = req.body;
        const userFound = await repository.findUserByEmail(email);
        if (userFound) {
            let authenticated = await passwordEncrypt.passwordAreEqual(password, userFound.password);
            if (authenticated) {
                const token = await security.generateToken({
                    id: userFound.id,
                    email: userFound.email
                });
                res.status(200).send({token: token, type: 'Bearer'})
            } else {
                res.status(401).send({message: `Senha incorreta ou invalida!`});
            }
        } else {
            res.status(401).send({message: `Não foi possivel encontrar usuário com e-mail igual a '${email}'`});
        }
    } catch (e) {
        res.status(500).send({message: e.message});
    }

};