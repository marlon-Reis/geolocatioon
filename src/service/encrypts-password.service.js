'use strict';

const bcrypt = require('bcryptjs');
const saltRounds = 10;

const salt = bcrypt.genSaltSync(saltRounds);

exports.encryptPassword = async (password) => {
    return await bcrypt.hashSync(password, salt);
};

exports.passwordAreEqual = async (passwordNotEncrypted, passwordEncrypted) => {
    return await bcrypt.compareSync(passwordNotEncrypted, passwordEncrypted);
};