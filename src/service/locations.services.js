'use strict';

const repository = require('../repository/locations.repository');
const userRepository = require('../repository/users.repository');

const locationDto = (req) => {
    return {
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        userId: undefined
    }

};

const responseDto = (location) => {
    return {
        latitude: location.latitude,
        longitude: location.longitude
    }
};

exports.create = async (req, res, next) => {
    const {id} = req.userContext;

    try {
        const location = locationDto(req);
        location.userId = id;
        let user = await userRepository.findUserById(id);
        if (user) {
            const response = await repository.create(location);
            res.status(201).send(responseDto(response));
        } else {

            responseUserNotFound(res, location.userId);
        }
    } catch (e) {
        errorInternalServer(res);
    }
};

exports.findAllLocations = async (req, res, next) => {
    try {
        const response = await repository.findAllLocations();
        res.status(200).send(response.map(responseDto));
    } catch (e) {
        errorInternalServer(res);
    }
};

exports.findAuthenticatedUserLocationByUserId = async (req, res, next) => {
    const {id} = req.userContext;
    try {
        const response = await repository.findLocationsByUserId(id);
        res.status(200).send(response.map(responseDto));
    } catch (e) {
        errorInternalServer(res);
    }
};

exports.findLocationsByUserUsername = async (req, res, next) => {
    const {username} = req.params;
    try {
        const user = await userRepository.findUserByUsername(username);
        if (user) {
            const response = await repository.findLocationsByUserId(user.id);
            res.status(200).send(response.map(responseDto));
        } else {
            responseUserNotFound(res,)
        }
    } catch (e) {
        res.status(404).send({
            message: `Não foi possivel encontrar localização do usuário com username = ${username}`
        });
    }
};

const errorInternalServer = (res) => {
    res.status(500).send({
        message: "Erro interno do servidor"
    });
};

const responseUserNotFound = (res, id) => {
    res.status(404).send({
        message: `Não foi possivel encontrar usuário com id = ${id}`
    });
};