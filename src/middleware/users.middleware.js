"use strict";

const {check, validationResult} = require('express-validator');
const repository = require('../repository/users.repository');

exports.check = [
    check('name').matches(/^[A-Z][a-zA-Z][^#&<>\\"~;$^%{}?]{1,20}$/).withMessage("Atributo é invalido!"),
    check('username').matches(/^[a-z0-9_-]{3,15}$/).withMessage("Atributo é invalido!"),
    check('password').matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).withMessage("Muito fraca, deve conter letras(maiúsculas e minúsculas),números e caracteres especiais!"),
    check("email").isEmail().withMessage("E-mail invalido!")
];

exports.checkUpdate = [
    check('name').matches(/^[A-Z][a-zA-Z][^#&<>\\"~;$^%{}?]{1,20}$/).withMessage("Atributo é invalido!"),
    check('password').matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).withMessage("Muito fraca, deve conter letras(maiúsculas e minúsculas),números e caracteres especiais!"),
    check("email").isEmail().withMessage("E-mail invalido!")
];

exports.validate = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        next();
    } else {
        return res.status(422).json({errors: errors.array()});
    }

};

exports.emailIsUnique = async (req, res, next) => {
    const {email} = req.body;
    repository.findUserByEmail(email).then((data, err) => {
        if (err) {
            res.status(500).json({message: err.message});
        }
        if (data) {
            res.status(422).json({
                param: 'email',
                value: email,
                message: `E-mail '${email}' já esta sendo utilizado por outro usuário!`
            });
        } else {
            next();
        }
    });
};


exports.usernameIsUnique = async (req, res, next) => {
    const {username} = req.body;
    repository.findUserByUsername(username).then((data, err) => {
        if (err) {
            res.status(500).json({message: err.message});
        }
        if (data) {
            res.status(422).json({
                param: 'username',
                value: username,
                message: `Username '${username}' já esta sendo utilizado por outro usuário!`
            });
        } else {
            next();
        }
    });
};


