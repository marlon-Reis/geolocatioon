"use strict";

const {check, validationResult} = require('express-validator');

exports.check = [
    check('latitude').isDecimal().withMessage("Atributo é invalido!"),
    check('longitude').isDecimal().withMessage("Atributo é invalido!"),
];

exports.validate = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        next();
    } else {
        return res.status(422).json({errors: errors.array()});
    }
};
