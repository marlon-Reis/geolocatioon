'use strict'

const Users = require('../model/users.model')();

Users.sync({force: false});

exports.findAllUsers = async () => {
    return await Users.findAll({});
};

exports.findUserById = async (id) => {
    return await Users.findOne({where: {id: id}})
};

exports.findUserByEmail = async (email) => {
    return await Users.findOne({
        where: {email: email}
    })
};

exports.findUserByUsername = async (username) => {
    return await Users.findOne({
        where: {username: username}
    })
};

exports.update = async (data) => {
    return await Users.update({
        name: data.name,
        email: data.email,
        password: data.password
    },{
        where: {id: data.id},
        returning: true,
        plain: true
    });
};

exports.deleteById = async (id) => {
    return await Users.destroy({
        where: {
            id: id
        }
    })
};

exports.create = async (data) => {
    return await Users.create(data);
};

