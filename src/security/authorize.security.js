const jose = require('jose');
const {
    JWE,
    JWK: {generateSync},
    JWS,
} = jose;


const key = generateSync('RSA', 4096, {alg: "RSA-OAEP-256", key_ops: ["encrypt'", "decrypt", "wrap"]});
const keyPublic = key.toPEM(false);
const keyPrivate = key.toPEM(true);

const decodeToken = async (tokenEncrypt) => {
    let token = await JWE.decrypt(tokenEncrypt, keyPrivate).toString();
    return await JWS.verify(token, keyPublic);
};

exports.authorize = async (req, res, next) => {
    let token = req.headers['authorization'];
    if (isValidToken(token)) {
        try {
            let data = await decodeToken(token.replace('Bearer ', '').trim());
            if (data) {
                req.userContext = data;
                next();
            } else {
                res.status(401).send({message: 'Token invalido'})
            }
        } catch (e) {
            console.log(e)
            res.status(401).send({message: 'Token invalido'});
        }
    } else {
        res.status(401).send({message: 'acesso negado'})
    }
};

const isValidToken = (token) => {
    if (token) {
        return token.startsWith("Bearer ");
    }
    return false;
};


exports.generateToken = async (data) => {
    let token = await JWS.sign(data, keyPrivate, {
        expiresIn: '1 hours',
        header: {
            typ: 'JWT'
        }
    });
    return await JWE.encrypt(token, keyPublic);


};