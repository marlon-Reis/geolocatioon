'use strict'

const Sequelize = require('sequelize');
const model = require("./model");
const Location = require('./location.model');

const data = {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    username: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    }
};


module.exports = () => {
    const Users = model('users', data);
    
    Users.associate = function (models) {
       // Users.belongsToMany(Location, { through: 'location_users', foreignKey: 'userId', as: 'locations' })
        Users.hasMany(Location,{ through: 'location_users', foreignKey: 'userId', as: 'locations' });
    };
    return Users;
}



