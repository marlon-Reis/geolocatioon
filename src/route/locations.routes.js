'use strict';

const router = require('./route');
const service = require('../service/locations.services');
const authorize = require('../security/authorize.security');
const middleware = require('../middleware/locations.middleware');

router.get('/locations', authorize.authorize, service.findAllLocations);

router.post('/locations',
    authorize.authorize,
    middleware.check,
    middleware.validate,
    service.create);

router.get('/locations/user/auth', authorize.authorize, service.findAuthenticatedUserLocationByUserId);
router.get('/locations/user/:username', 
//authorize.authorize, 
service.findLocationsByUserUsername);

module.exports = router;