'use strict';

const router = require('./route');
const service = require('../service/users.service');
const authorize = require('../security/authorize.security');
const middleware = require('../middleware/users.middleware');

router.get('/users', authorize.authorize, service.findAllUsers);

router.post('/users',
    middleware.check,
    middleware.validate,
    middleware.emailIsUnique,
    middleware.usernameIsUnique,
    service.create);

router.put('/users', authorize.authorize,
    middleware.checkUpdate,
    middleware.validate,
    service.update);

router.get('/users/info', authorize.authorize, service.findUserById);

router.delete('/users', authorize.authorize, service.deleteById);

module.exports = router;

