'use strict';

const router = require('./route');
const service = require('../service/authentication.service');

router.post('/auth/login', service.authentication);

module.exports = router;